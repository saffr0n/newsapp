from django.urls import path

from . import views

app_name = 'newsapp'

urlpatterns = [
    path('',  views.show_roll, name='index'),
    path('roll', views.show_roll, name='roll'),
    path('post/<int:post_id>', views.show_post, name='post'),
]