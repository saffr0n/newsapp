import json
from datetime import datetime
from time import sleep

import urllib3
from bs4 import BeautifulSoup
from django.shortcuts import render
from django.db import IntegrityError
from threading import Thread

from .models import Post


meduza = 'https://meduza.io/'
meduza_rss = meduza + 'rss/{}'
meduza_api = meduza + 'api/v3/{}'


def get_post(post_link):
    """
    Get post from API and parse it to dict
    :param post_link: url to get post from api
    :return: result post dict
    """
    http = urllib3.PoolManager()
    response = http.request('GET', meduza_api.format(post_link))
    post = json.loads(response.data)['root']
    return {'title': post['title'],
            'text': post['content'].get('body'),
            'pub_date': post['pub_date'],
            }


def get_roll(feed='all'):
    """
    Get rss list of News from API and join it with
    info about every post in that list
    """
    news = []
    http = urllib3.PoolManager()
    xml = http.request('GET', meduza_rss.format(feed))
    soup = BeautifulSoup(xml.data)
    for item in soup.findAll('item'):
        news.append({'api_post_link': item.find('guid').string.replace(meduza, ''),
                      'img_url': item.find('enclosure')['url'],
                      })
        post = get_post(news[-1]['api_post_link'])
        news[-1]['title'] = post['title']
        news[-1]['text'] = post['text'].replace('src="/image/', 'src="//meduza.io/image/') if post['text'] else None
        news[-1]['pub_date'] = post['pub_date']
    return news


def show_post(request, post_id):
    return render(request,
                  'newsapp/post.html',
                  {'post': Post.objects.get(id=post_id)})


def show_roll(request):
    news = Post.objects.all()
    return render(request,
                  'newsapp/roll.html',
                  {'news': news})


def update_local_news():
    """
    Function gets list of News every 10 minuts and save it in DB if it alredy not in
    """
    while True:
        for post in get_roll():
            try:
                Post.objects.create(title=post['title'],
                                    pub_date=datetime.strptime(post['pub_date'], '%Y-%m-%d'),
                                    text=post['text'],
                                    )
            except IntegrityError:
                    continue
        sleep(600)


news_saver = Thread(target=update_local_news)
news_saver.start()