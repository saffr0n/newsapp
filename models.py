from django.db import models


class Post(models.Model):
    title = models.CharField(max_length=200, unique=True)
    save_date = models.DateField(auto_now_add=True)
    pub_date = models.DateField()
    text = models.CharField(max_length=90000, null=True)
