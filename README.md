# Django приложение для простмотра новостной ленты сайта Meduza

## Устанвка в проект: 

#### 1) Клонируйте репозиторий в папку с проектом

#### 2) В yourproject/urls.py добавьте строку в urlpatterns:


```
    urlpatterns = [
        ...
        url(r'^newsapp/', include('newsapp.urls')),
    ]
```


#### 3) В settings.py добавьте строку с названием приложения в INSTALLED_APPS:


```
    INSTALLED_APPS = (
        ...
        'newsapp',
    )
```

#### 4) Установите необходимые зависимости:

```
    pip install -r requirenments
```

#### 5) Обновите структуру базы данных:

```
    python manage.py makemigrations
    python manage.py migrate
```

#### Yовости будут доступны по ссылке: 
> {yourdomen}/newsapp/roll